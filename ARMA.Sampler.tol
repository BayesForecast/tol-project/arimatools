//////////////////////////////////////////////////////////////////////////////
// FILE   : @ARMA.Sampler.tol
// PURPOSE: Class for ARMA MH sampling for a given structure of factors with
// fixed AR, MA and DIF degrees 
//////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////
Class @ARMA.Sampler : @ArimaVector, @MHSampler
//////////////////////////////////////////////////////////////////////////////
{
  Static Real PartialDataSample.Ratio = 0.5;
  Static NameBlock Options = 
  [[
     NameBlock Evaluator = [[
       Real Levinson = 1;
       Real PartialLevinson = 2;
       Real Almagro = 3;
       Real DifEq = 4;
       Real GohbergSemencul = 5;
       Real Backasting = 6;
       Real FastChol = 7;
       Real FastCholSea = 8;
       Real ACF = 9;
       Real FactorACF = 10
     ]];
     NameBlock Optimizer = [[
       Real NonLinGloOpt = 1;
       Real Marquardt = 2
     ]]
  ]];
  NameBlock config = [[Real _.undefined=? ]];

  ////////////////////////////////////////////////////////////////////////////
  Real setDefaultConfig(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    NameBlock config :=
    [[
       Real Evaluator = 
         ArimaTools::@ARMA.Sampler::Options::Evaluator::Levinson;
       Real Optimizer = 
         ArimaTools::@ARMA.Sampler::Options::Optimizer::NonLinGloOpt;
       Real NonLinGloOpt.Algorithm = 
         NonLinGloOpt::Algorithm::LN_SBPLX;
       Real nonStatProbSampleLength = 1000
    ]];
    True
  };
  @Matrix _.noise;
  @TimeSet _.dating = @TimeSet(C);
  @Date _.lastDate = @Date(Today);

  Text _.label;
  Set _.degStr = Copy(Empty);
  Matrix _.degVec = Constant(0,0,?);
  Matrix _.difNoise = Constant(0,0,?);
  Set _.eval = Copy(Empty);
  Set _.period = Copy(Empty);
  Real _.s = ?;
  Real _.p = ?;
  Real _.q = ?;
  Real _.d = ?;
  Real _.N = ?;
  Real _.Nsub = ?;
  Set _.optimizer = Copy(Empty);
  Real _.priorLogDens = 0;
  Real _.stationaryLogDens = 0;

  // @ARMA.Splitter.ACF : 
  Set _.factor.acf = Copy(Empty);

  Real _.nonStationaryProb = ?;

  ////////////////////////////////////////////////////////////////////////////
  Static Set DegVecToDegStr(Set period, Matrix decVec)
  ////////////////////////////////////////////////////////////////////////////
  {
    For(1,Card(period), Set(Real k)
    {
      Real base = (k-1)*3;
      @ARIMADegreeStruct
      (
        period[k],
        MatDat(decVec,base+1,1),
        MatDat(decVec,base+2,1),
        MatDat(decVec,base+3,1)
      )
    })
  };

  ////////////////////////////////////////////////////////////////////////////
  Static Matrix DegStrToDegVec(Set degStr)
  ////////////////////////////////////////////////////////////////////////////
  {
    SetCol(SetConcat(EvalSet(degStr, Set(@ARIMADegreeStruct f)
    {
      [[f->ARDegree, f->MADegree, f->DIFDegree ]]
    })))
  };

  ////////////////////////////////////////////////////////////////////////////
  Static Set ArimaToPeriod(Set arima)
  ////////////////////////////////////////////////////////////////////////////
  {
    EvalSet(arima,Real(@ARIMAStruct f) { f->Periodicity })
  };

  ////////////////////////////////////////////////////////////////////////////
  Static Set ArimaToDegStr(Set arima)
  ////////////////////////////////////////////////////////////////////////////
  {
    EvalSet(arima, @ARIMADegreeStruct(@ARIMAStruct f)
    {
      @ARIMADegreeStruct
      (
        f->Periodicity,
        Degree(f->AR),
        Degree(f->MA),
        Degree(f->DIF)
      )
    })
  };

  ////////////////////////////////////////////////////////////////////////////
  Static Set DegStrToArima(Set degStr)
  ////////////////////////////////////////////////////////////////////////////
  {
    EvalSet(degStr, @ARIMAStruct(@ARIMADegreeStruct f)
    {
      @ARIMAStruct
      (
        f->Periodicity,
        RandStationary(f->ARDegree,f->Periodicity),
        RandStationary(f->MADegree,f->Periodicity),
        (1-B^f->Periodicity)^f->DIFDegree
      )
    })
  };

  ////////////////////////////////////////////////////////////////////////////
  Static @ARMA.Sampler CreateFromLabel(Matrix noise, Text label)
  ////////////////////////////////////////////////////////////////////////////
  {
    @ARMA.Sampler armaSampler = 
    [[
      @Matrix _.noise = [[noise]];
      Text _.label = Copy(label);
      Set _.arima = GetArimaFromLabel(_.label);
      Set _.period = @ARMA.Sampler::ArimaToPeriod(_.arima);
      Set _.degStr = @ARMA.Sampler::ArimaToDegStr(_.arima);
      Matrix _.decVec = @ARMA.Sampler::DegStrToDegVec(_.degStr)
    ]];
    Real armaSampler::initialize(?);
    Set armaSampler::_.arima := 
      armaSampler::vector2arima(Rand(armaSampler::_.varNum,1,-2,2));
    armaSampler
  };

  ////////////////////////////////////////////////////////////////////////////
  Static @ARMA.Sampler CreateFromArima(Matrix noise, Set arima)
  ////////////////////////////////////////////////////////////////////////////
  {
    @ARMA.Sampler armaSampler = 
    [[
      @Matrix _.noise = [[noise]];
      Set _.arima = DeepCopy(arima);
      Set _.period = @ARMA.Sampler::ArimaToPeriod(_.arima);
      Text _.label = GetLabelFromArima(_.arima);
      Set _.degStr = @ARMA.Sampler::ArimaToDegStr(_.arima);
      Matrix _.decVec = @ARMA.Sampler::DegStrToDegVec(_.degStr)
      
    ]];
    Real armaSampler::initialize(?);
    armaSampler
  };

  ////////////////////////////////////////////////////////////////////////////
  Static @ARMA.Sampler CreateFromDegStr(Matrix noise, Set degStr)
  ////////////////////////////////////////////////////////////////////////////
  {
    @ARMA.Sampler armaSampler = 
    [[
      @Matrix _.noise = [[noise]];
      Set _.degStr = DeepCopy(degStr);
      Set _.arima = @ARMA.Sampler::DegStrToArima(degStr);
      Set _.period = @ARMA.Sampler::ArimaToPeriod(_.arima);
      Text _.label = GetLabelFromArima(_.arima);
      Matrix _.decVec = @ARMA.Sampler::DegStrToDegVec(_.degStr)
    ]];
    Real armaSampler::initialize(?);
    armaSampler
  };

  ////////////////////////////////////////////////////////////////////////////
  Static @ARMA.Sampler CreateFromDegVec(Matrix noise, Set period, Matrix degVec)
  ////////////////////////////////////////////////////////////////////////////
  {
    @ARMA.Sampler armaSampler = 
    [[
      @Matrix _.noise = [[noise]];
      Set _.period = DeepCopy(period);
      Matrix _.degVec = Copy(degVec);
      Set _.degStr = @ARMA.Sampler::DegVecToDegStr(period, degVec);
      Set _.arima = @ARMA.Sampler::DegStrToArima(_.degStr);
      Text _.label = GetLabelFromArima(_.arima)
    ]];
    Real armaSampler::initialize(?);
    armaSampler
  };

  ////////////////////////////////////////////////////////////////////////////
  Real initialize(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    Real setDefaultConfig(?);
    Real initialize.ArimaVector(?);
    Set _.period := EvalSet(_.arima,Real(@ARIMAStruct f) { f->Periodicity });
    Polyn ar = ARIMAGetAR(_.arima);
    Polyn ma = ARIMAGetMA(_.arima);
    Polyn dif = ARIMAGetDIF(_.arima);
    Real _.s := SetMax(_.period);
    Real _.p := Degree(ar);
    Real _.q := Degree(ma);
    Real _.d := Degree(dif);
    Real _.N := Rows($_.noise)-_.d;
    Real _.Nsub := Round(@ARMA.Sampler::PartialDataSample.Ratio * _.N);
    Matrix _.degVec := SetCol(SetConcat(EvalSet(_.degStr, Set(@ARIMADegreeStruct f)
    {
      [[f->ARDegree, f->MADegree, f->DIFDegree ]]
    })));
    Set _.varName := SetConcat(For(1,_.factorNum,Set(Real k)
    {
      Real s = _.period[k];
      Text prefix = "F"<<k+".P"<<s+".";
      @Set f = [[ _.arima[k] ]];
      For(1,Degree($f->AR)/s,Text(Real j) { prefix+"AR"<<(j*s) }) <<
      For(1,Degree($f->MA)/s,Text(Real j) { prefix+"MA"<<(j*s) })
    }));
    Real _.varNum := Card(_.varName);
    Real _.priorLogDens := -0.5*_.varNum*Log(_.N);
    Real _.stationaryLogDens = 
      ArimaTools::UnitRoot::LogVRegion(_.p)+
      ArimaTools::UnitRoot::LogVRegion(_.q);
    Serie difNoise = dif:MatSerSet(Tra($_.noise),C,Today)[1];
    Matrix _.difNoise := Tra(SerSetMat([[difNoise]]));
    True
  };
     
  ////////////////////////////////////////////////////////////////////////////
  Real log_lk_target(Matrix y, Matrix grad)
  ////////////////////////////////////////////////////////////////////////////
  {
    Real lnL = pi.logDens(y);
    Matrix If(Rows(grad), grad:=Gradient(pi.logDens,y) );
    lnL
  };

  ////////////////////////////////////////////////////////////////////////////
  Real optimize.NonLinGloOpt(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    @NameBlock aref = [[_this]]; 
    NameBlock optimizer = NonLinGloOpt::@PipeLine::undefined(?);
    Matrix optimizer::x0 := $aref::_.x;
    Real optimizer::set_problem(NonLinGloOpt::@Problem problem = [[
      Real sign = 1;
      Real neededGlobal = False;
      Real id_analyticalClass = 2;
      Real id_useGradient = True;
      Real n = $aref::_.varNum;
      Anything target = [[$aref, log_lk_target ]];
      Set inequations = Copy(Empty)
    ]]);
    Real optimizer::add_method(NonLinGloOpt::@Method method =
    [[
      Real id_algorithm = config::NonLinGloOpt.Algorithm
    ]]);
    Set sc = @NameBlock((optimizer::methods[1])::stopCriteria);
  //Real $sc::absoluteTolerance_target := 1E-15;
  //Real $sc::relativeTolerance_target := 1E-15;
  //Real $sc::relativeTolerance_var := 1E-15;
    Real (optimizer::problems[1])::check(?);
    Real optimizer::optimize(False);
    Matrix _.mle := optimizer::x.opt;
    Set _.arima := vector2arima(optimizer::x.opt);
    True    
  };

  ////////////////////////////////////////////////////////////////////////////
  Real optimize.Marquardt(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    Set arimaTr = Traspose(_.arima);
    Date first = Succ($_.lastDate,$_.dating,1-Rows($_.noise));
    Set modelDef = @ModelDef( 
      Serie Output = MatSerSet(Tra($_.noise),$_.dating,first)[1]; 
      Real FstTransfor = 1; 
      Real SndTransfor = 0; 
      Real Period = SetMax(arimaTr[1]); 
      Real Constant = 0; 
      Polyn Dif = SetProd(arimaTr[4]); 
      Set AR = arimaTr[2]; 
      Set MA = arimaTr[3]; 
      Set Input = Copy(Empty); 
      Set NonLinInput = Copy(Empty) 
    );
    Set modelEst = Estimate(modelDef);
    Set res = modelEst::Definition; 
    Set For(1,_.factorNum,Real(Real f)
    {
      Polyn _.arima[f]->AR := (res->AR)[f];
      Polyn _.arima[f]->MA := (res->MA)[f];
      f
    });
    Set _.optimizer := @NameBlock(NameBlock model = 
    [[
      Set def = modelDef;
      Set est = modelEst
    ]]);
    Matrix _.mle := arima2vector(_.arima);
    True
  };

  ////////////////////////////////////////////////////////////////////////////
  Real optimize(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    Real setEvalMethod(?);
    Matrix _.x := _.mle;
    If(Or(_.p,_.q),
    {
      If(config::Optimizer==
         ArimaTools::@ARMA.Sampler::Options::Optimizer::NonLinGloOpt,
      {
        optimize.NonLinGloOpt(?) 
      },
      {
        optimize.Marquardt(?) 
      })
    });
    Matrix _.mle := arima2vector(_.arima);
    Matrix _.x := _.mle;
    Set _.eval := evalLogLikelihood(_.mle);
    Real _.llh.max := _.eval::logLikelihood;
    True
  };

  ////////////////////////////////////////////////////////////////////////////
  Set DrawStationary(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    For(1,_.factorNum, @ARIMAStruct(Real k)
    {
      @Set f = [[ _.arima[k] ]];
      Real s = $f->Periodicity;
      Real p = Degree($f->AR)/s;
      Real q = Degree($f->MA)/s;
      @ARIMAStruct( 
        $f->Periodicity,
        Polyn AR = RandStationary(p, s);
        Polyn MA = RandStationary(q, s);
        $f->DIF )
    })
  };

  ////////////////////////////////////////////////////////////////////////////
  Set evalLogLikelihood_Levinson(Set arima)
  ////////////////////////////////////////////////////////////////////////////
  {
    Set ev = ARIMALevinsonEval(arima, $_.noise, False, ?);
    Real logLikelihood = ev[12];
    [[ logLikelihood ]] << ev
  };
    
  ////////////////////////////////////////////////////////////////////////////
  Set evalLogLikelihood_PartialLevinson(Set arima)
  ////////////////////////////////////////////////////////////////////////////
  {
    Real t0 = IntRand(1,1+_.N-_.Nsub);
    Set rowIdx = Range(t0,t0+_.Nsub-1,1);
    Matrix noise.sub = SubRow($_.noise,rowIdx);
    Set ev = ARIMALevinsonEval(arima, noise.sub, False, ?);
    Real logLikelihood = ev[12];
    [[ logLikelihood ]] << ev
  };
    
  ////////////////////////////////////////////////////////////////////////////
  Set evalLogLikelihood_Almagro(Set arima)
  ////////////////////////////////////////////////////////////////////////////
  {
    Set ev = ARIMAAlmagroEval(arima, $_.noise, ?);
    Real logLikelihood = ev[9];
    [[ logLikelihood ]] << ev
  };

  ////////////////////////////////////////////////////////////////////////////
  Set evalLogLikelihood_GohbergSemencul(Set arima)
  ////////////////////////////////////////////////////////////////////////////
  {
    Polyn ar = ARIMAGetAR(arima);
    Polyn ma = ARIMAGetMA(arima);
    Polyn dif = ARIMAGetDIF(arima);
    Set ev = ARMAGohbergSemenculEval(ar, ma, _.difNoise);
    Real logLikelihood = -0.5*MatDat(ev[1],1,1);
    [[ logLikelihood ]] << ev
  };
    
  ////////////////////////////////////////////////////////////////////////////
  Set evalLogLikelihood_DifEq(Set arima)
  ////////////////////////////////////////////////////////////////////////////
  {
    Polyn ar = ARIMAGetAR(arima);
    Polyn ma = ARIMAGetMA(arima);
    Date first = Succ($_.lastDate,$_.dating,1-Rows(_.difNoise));
    Serie w = MatSerSet(Tra(_.difNoise),$_.dating,first)[1];
    Serie a = DifEq(ar/ma,w);
    Real s2 =  AvrS(a^2);
    Real logLikelihood =  -0.5*_.N*Log(2*Pi)-0.5*Log(s2);
//  WriteLn("TRACE [evalLogLikelihood_DifEq] arima:\n"<<arima+" s2="<<s2+" logLikelihood(-0.5*sum(a^2)="<<logLikelihood);
    [[ logLikelihood, w, a, s2 ]]
  };
    
  ////////////////////////////////////////////////////////////////////////////
  Set evalLogLikelihood_Backasting(Set arima)
  ////////////////////////////////////////////////////////////////////////////
  {
    Polyn ar = ARIMAGetAR(arima);
    Polyn ma = ARIMAGetMA(arima);
    Set ev = Backasting(ar/ma, _.difNoise);
    Real s2 =  MatAvr(ev[1]$*ev[1]);
    Real logLikelihood =  -0.5*_.N*Log(2*Pi)-0.5*Log(s2);
    [[ logLikelihood ]] << ev
  };
    
  ////////////////////////////////////////////////////////////////////////////
  Set evalLogLikelihood_FastChol(Set arima)
  ////////////////////////////////////////////////////////////////////////////
  {
    VMatrix _.W = Mat2VMat(_.difNoise);
    Polyn ar = ARIMAGetAR(arima);
    Polyn ma = ARIMAGetMA(arima);
    NameBlock fc = ArimaTools::ARMAProcess::FastCholeskiCovFactor(ar, ma, _.N);
    Real logLikelihood = If(Or(IsUnknown(fc::_.logDetCov), 
                            fc::_.logDetCov==-1/0), -1/0,
    {
      VMatrix a = CholeskiSolve(fc::_.L_ma,fc::_.Li_ar*_.W,"PtL");
      Real ata = VMatSum(a$*a);
      Real s2 = ata / _.N;
      -0.5*_.N*Log(2*Pi*s2)
    });
    [[logLikelihood]]
  };
    

  ////////////////////////////////////////////////////////////////////////////
  Set evalLogLikelihood_FastCholSea(Set arima)
  ////////////////////////////////////////////////////////////////////////////
  {
    VMatrix a = Mat2VMat(_.difNoise);
    Real m = VRows(a);
    Set Li_ar_factors = Copy(Empty);
    Set L_ma_factors  = Copy(Empty);
    Real f = 1;
    Real fail = False;
    While(And(!fail,f<=_.factorNum),
    {
      NameBlock fc  = ArimaTools::ARMAProcess::FastCholeskiCovFactor(
        arima[f]->AR, arima[f]->MA, m);
      Real fail := Or(IsUnknown(fc::_.logDetCov),fc::_.logDetCov==-1/0);
      VMatrix If(!fail,
         a := CholeskiSolve(fc::_.L_ma,fc::_.Li_ar*a,"PtL"));
      Real f := f+1
    });
    Real logLikelihood = If(fail, -1/0,
    {
      Real ata = VMatSum(a$*a);
      Real s2 = ata / _.N;
      -0.5*_.N*Log(2*Pi)-0.5*Log(s2)
    });
    [[logLikelihood]]
  };
    
  ////////////////////////////////////////////////////////////////////////////
  Set evalLogLikelihood_ACF(Set arima)
  ////////////////////////////////////////////////////////////////////////////
  {
    Real logLikelihood = $_.factor.acf::pi.logDens(arima);
    [[logLikelihood]]
  };
    
  ////////////////////////////////////////////////////////////////////////////
  Set evalLogLikelihood_FactorACF(Set arima)
  ////////////////////////////////////////////////////////////////////////////
  {
    Real logLikelihood = $_.factor.acf::pi.logDens.sea(arima);
    [[logLikelihood]]
  };
    
  ////////////////////////////////////////////////////////////////////////////
  Set evalLogLikelihood_str(Set arima)
  ////////////////////////////////////////////////////////////////////////////
  {
    Set ev = Case(
    config::Evaluator == 
       ArimaTools::@ARMA.Sampler::Options::Evaluator::Levinson,
    evalLogLikelihood_Levinson(arima),
    config::Evaluator == 
       ArimaTools::@ARMA.Sampler::Options::Evaluator::PartialLevinson,
    evalLogLikelihood_PartialLevinson(arima),
    config::Evaluator == 
       ArimaTools::@ARMA.Sampler::Options::Evaluator::Almagro,
    evalLogLikelihood_Almagro(arima),
    config::Evaluator == 
       ArimaTools::@ARMA.Sampler::Options::Evaluator::DifEq,
    evalLogLikelihood_DifEq(arima),
    config::Evaluator == 
       ArimaTools::@ARMA.Sampler::Options::Evaluator::GohbergSemencul,
    evalLogLikelihood_GohbergSemencul(arima),
    config::Evaluator == 
       ArimaTools::@ARMA.Sampler::Options::Evaluator::Backasting,
    evalLogLikelihood_Backasting(arima),
    config::Evaluator == 
       ArimaTools::@ARMA.Sampler::Options::Evaluator::FastChol,
    evalLogLikelihood_FastChol(arima),
    config::Evaluator == 
       ArimaTools::@ARMA.Sampler::Options::Evaluator::FastCholSea,
    evalLogLikelihood_FastCholSea(arima),
    config::Evaluator == 
       ArimaTools::@ARMA.Sampler::Options::Evaluator::ACF,
    evalLogLikelihood_ACF(arima),
    config::Evaluator == 
       ArimaTools::@ARMA.Sampler::Options::Evaluator::FactorACF,
    evalLogLikelihood_FactorACF(arima));
/*
    Real ev::logLikelihood := 
      ev::logLikelihood + 
      _.priorLogDens +
      _.stationaryLogDens;
*/
    ev
  };

  ////////////////////////////////////////////////////////////////////////////
  Set evalLogLikelihood(Matrix z)
  ////////////////////////////////////////////////////////////////////////////
  {
    Set arima.z = vector2arima(z);
    evalLogLikelihood_str(arima.z)
  };


  ////////////////////////////////////////////////////////////////////////////
  Real pi.logDens(Matrix y)
  ////////////////////////////////////////////////////////////////////////////
  {
    Set ev = evalLogLikelihood(y);
    ev::logLikelihood
  };

  ////////////////////////////////////////////////////////////////////////////
  Real pi.logDens.sttCumAvr(Matrix z, Real llh.z)
  ////////////////////////////////////////////////////////////////////////////
  {
    Real r = Rows(_.mcmc.llh);
    Real If(llh.z > _.llh.max,
    {
      Matrix _.mle := z;
      Real _.llh.max := llh.z
    });
    Real If(llh.z>-1/0,
    {
      Real AppendRows(_.mcmc,Tra(z));
      Real AppendRows(_.mcmc.llh,Col(llh.z));
      Real _.llh.avr := If(r==1,llh.z,
        Log((Exp(_.llh.avr-_.llh.max)*r + Exp(llh.z-_.llh.max))/(r+1))+_.llh.max)
  //Set View([["TRACE pi.logDens.rndSttCumAvr",llh.z,r,_.llh.max,_.llh.avr,"\n\n"]],"Std");
    });
    Copy(_.llh.avr)
  };


  ////////////////////////////////////////////////////////////////////////////
  Real pi.logDens.sttCumAvr.draw(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    Set arima = DrawStationary(?);
    Matrix z = arima2vector(arima);
    Real llh = pi.logDens(z);
    Real pi.logDens.sttCumAvr(z,llh);
    llh
  };

  ////////////////////////////////////////////////////////////////////////////
  Real postDraw(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    True
  };

  ////////////////////////////////////////////////////////////////////////////
  Real post_build_mcmc(Real sampleLength)
  ////////////////////////////////////////////////////////////////////////////
  {
    Set _.arima := vector2arima(_.mle);
    Set _.eval := evalLogLikelihood(_.mle);
    Real _build_sample(?);
    Set _.histogram := For(1, _.varNum, Matrix(Real k)
    {
      Matrix freq = Frequency(_.sample[k],Round(Sqrt(sampleLength)));
      Eval("Histogram."+_.varName[k]+"=freq")
    });
    True
  };

  ////////////////////////////////////////////////////////////////////////////
  Real build_mcmc(Real sampleLength)
  ////////////////////////////////////////////////////////////////////////////
  {
    Real _build_mcmc(sampleLength);
    Real post_build_mcmc(sampleLength);
    True
  };
  

  ////////////////////////////////////////////////////////////////////////////
  Real NonStationarityProb(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    Real n = _.varNum;
    Real oldEv = config::Evaluator; 
    Matrix oldMle = _.mle;
    Real setMetric.FreeParam(?);
    Matrix _.x = arima2vector(_.arima);
    Real config::Evaluator := 
      ArimaTools::@ARMA.Sampler::Options::Evaluator::DifEq;
    Real prob = If(!n,0, 
    {
      Real optimize.NonLinGloOpt(?);
      Matrix x = _.mle;
      Matrix G = Gradient(pi.logDens,x);
      Matrix H = -Hessian(pi.logDens,x);
      WriteLn("TRACE [NonStationarityProb] x:\n"<<Matrix Tra(x));
      WriteLn("TRACE [NonStationarityProb] G:\n"<<Matrix Tra(G));
      WriteLn("TRACE [NonStationarityProb] H:\n"<<H);
      If(Or(Rows(H)!=_.varNum,Columns(H)!=_.varNum,HasUnknown(H)), 1,
      { 
        Set svd = SVD(H);
        Matrix U  = svd[1];
        Matrix D2 = svd[2];
        Matrix V  = svd[3];
        Matrix D  = RPow(D2,0.5);
        Real D.logDet = MatSum(Log(SubDiag(D,0)));
        Matrix Di = InverseDiag(D);
        Matrix S.Lt = V*Di;
        Matrix pql.sample = SetMat(For(1,config::nonStatProbSampleLength,Set(Real iter)
        {
          Matrix e = Gaussian(n,1,0,1);
          Matrix z = x + S.Lt * e;
          Set arima = vector2arima(z);
          Real isNonStationary = !SetProd(EvalSet(arima,Real(@ARIMAStruct factor)
          {
            And(IsStationary(factor->AR),IsStationary(factor->MA))
          }));
        //WriteLn("TRACE [NonStationarityProb] x:\n"<<Matrix Tra(z));
        //WriteLn("TRACE [NonStationarityProb] arima:\n"<<arima);
        //WriteLn("TRACE [NonStationarityProb] isNonStationary:"<<isNonStationary);
          [[
            isNonStationary
/*
            Real q = -0.5*n*Log(2*Pi)+D.logDet-0.5*MatSum(e^2);
            Real p = pi.logDens(z);
            Real l = p-q;
*/
          ]]
        }));
  /*
        Matrix q = SubCol(pql.sample,[[2]]);
        Matrix _.mcmc.llh := SubCol(pql.sample,[[4]]);
        Real llh.max_ = MatMax(_.mcmc.llh);
        Matrix _.mcmc.prob := Exp(_.mcmc.llh-llh.max_);
        Real _.llh.avr := Log(MatAvr(_.mcmc.prob))+llh.max_;
        Copy(_.llh.avr)
  */
        MatAvr(SubCol(pql.sample,[[1]]))
      })
    });
    Real _.nonStationaryProb := prob;
    WriteLn("  Non Stationarity Probability : "<< _.nonStationaryProb);
    Matrix _.mle := oldMle;
    Real config::Evaluator := oldEv;
    Real setMetric.StationaryForced(?);
    prob
  };

  ////////////////////////////////////////////////////////////////////////////
  Real clear(Real void)
  ////////////////////////////////////////////////////////////////////////////
  { 
    Set _.noise := Copy(Empty); 
    Set _.dating := Copy(Empty); 
    Set _.lastDate := Copy(Empty); 
    Set _.optimizer := Copy(Empty);
    True
  };

  ////////////////////////////////////////////////////////////////////////////
  Real __destroy(Real void)
  ////////////////////////////////////////////////////////////////////////////
  { 
    clear(void)
  }

};

