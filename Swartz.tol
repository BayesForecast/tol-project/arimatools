//////////////////////////////////////////////////////////////////////////////
// FILE:    general.tol
// PURPOSE: General tools for ARIMA analysis
//////////////////////////////////////////////////////////////////////////////

NameBlock Swartz = 
[[
Text _.autodoc.member.Identify.Options ="Default ARIMA options for model "
"identification";
Set Identify.Options = [[ "P1DIF0AR0MA1", "P1DIF1AR0MA1", "P1DIF0AR0MA1.2", "P1DIF1AR0MA1.2",
			"P1DIF0AR1MA1", "P1DIF1AR1MA1", "P1DIF0AR1MA0", "P1DIF0AR1.2MA0", 
			"P1DIF1AR1MA0", "P1DIF1AR1.2MA0" ]];

////////////////////////////////////////////////////////////////////////////////
// Generate Periodic Polynomials
Set _GeneratePeriodic(Set Periods) {
////////////////////////////////////////////////////////////////////////////////

	Set For(1, Card(Identify.Options), Set (Real id) {

		Polyn Diff = 1;

		Set For(1, Card(Periods), Polyn (Real conta) {
				
			Polyn Diff := Diff*(1-B^Periods[conta])
		});

		Polyn Generic = 1;

		Set For(1, Card(Periods), Polyn (Real conta) {
				
			Polyn Generic := Generic*(1-0.1*B^Periods[conta])
		});

		Eval("Set Polynomials"+FormatReal(id)+" = [[Diff, Generic]]")		

	})

};

//////////////////////////////////////////////////////////////////////////////
Text _.autodoc.member.Identify ="Fun��o de Detec��o de ARIMA "
"Dado um conjunto de outputs, retorna o melhor ARIMA "
"Testa o melhor ARIMA, comparando os coeficientes Swartz";
Set Identify(Serie output, Set inputs_set, Real BoxCoxTransform0, Real BoxCoxTransform1, Set Periods)
//////////////////////////////////////////////////////////////////////////////
{

// Given a Regular ARIMA Set, it tries do define all the most common possible combinations
// of the seasonal part of the polynomial

	Set Periodic_Polyn = _GeneratePeriodic(Set Periods);

	Set Models_ARIMA_IMA_E = Set For(1, Card(Identify.Options), Set (Real conta) {

			Set Arima_From_Label = GetArimaFromLabel(Identify.Options[conta]);

			Set Modelo_Esqueleto = ModelDef
			(
 				output, // Serie Output
 				BoxCoxTransform0,      // Primera trasnformacion Box-Cox              
 				BoxCoxTransform1,      // Segunda transformacion Box-Cox              
 				SetMax(Periods),      // Periodo, maximo periodo.            
 				0,      // Constante              
 				((Arima_From_Label[1][4])*Periodic_Polyn[conta][1]),    // Polinomio de diferencias               
 				SetOfPolyn(Arima_From_Label[1][2], 1),  // Set AR Regular
 				SetOfPolyn(Arima_From_Label[1][3], Periodic_Polyn[conta][2]),  // Set MA Regular y Estacional
 				inputs_set, // Inputs
 				AllLinear // Colocar siiempre como AllLinear
			)

	});


	Set Models_ARIMA_AR_E = Set For(1, Card(Identify.Options), Set (Real conta) {

			Set Arima_From_Label = GetArimaFromLabel(Identify.Options[conta]);

			Set Modelo_Esqueleto = ModelDef
			(
 				output, // Serie Output
 				BoxCoxTransform0,      // Primera trasnformacion Box-Cox              
 				BoxCoxTransform1,      // Segunda transformacion Box-Cox              
 				SetMax(Periods),      // Periodo, maximo periodo.            
 				0,      // Constante              
 				(Arima_From_Label[1][4]),    // Polinomio de diferencias               
 				SetOfPolyn(Arima_From_Label[1][2], Periodic_Polyn[conta][2]),  // Set AR Regular y Estacional
 				SetOfPolyn(Arima_From_Label[1][3], 1),  // Set MA Regular
 				inputs_set, // Inputs
 				AllLinear // Colocar siiempre como AllLinear
			)

	});

        Set Models_ARIMA_ARIMA_E = Set For(1, Card(Identify.Options), Set (Real conta) {

			Set Arima_From_Label = GetArimaFromLabel(Identify.Options[conta]);

			Set Modelo_Esqueleto = ModelDef
			(
 				output, // Serie Output
 				BoxCoxTransform0,      // Primera trasnformacion Box-Cox              
 				BoxCoxTransform1,      // Segunda transformacion Box-Cox              
 				SetMax(Periods),      // Periodo, maximo periodo.            
 				0,      // Constante              
 				((Arima_From_Label[1][4])*Periodic_Polyn[conta][1]),    // Polinomio de diferencias               
 				SetOfPolyn(Arima_From_Label[1][2], Periodic_Polyn[conta][2]),  // Set AR Regular y Estacional
 				SetOfPolyn(Arima_From_Label[1][3], Periodic_Polyn[conta][2]),  // Set MA Regular y Estacional
 				inputs_set, // Inputs
 				AllLinear // Colocar siiempre como AllLinear
			)

	});
	

	Set Models_ARIMA = Models_ARIMA_IMA_E << Models_ARIMA_AR_E << Models_ARIMA_ARIMA_E;

// Faz as estima��es, 1 a 1

	Set SwartzSet = Set For(1, Card(Models_ARIMA), Set (Real conta) { 
		Set Estimation = Estimate(Models_ARIMA[conta], First(output), Last(output))
	});

// Seleciona a estima��o com o melhor coeficiente "Swartz"

	Real min_i = 1;
	Real min_svars = 0;

	Real Crescente(Real x, Real y)
	{
		Sign(x-y)
	};

	Set MinSwartzSet = Sort(SwartzSet, Real (Set x1, Set x2) { 
		Crescente(x1[1][13], x2[1][13])
	});

	Set output = MinSwartzSet[1]
};

//////////////////////////////////////////////////////////////////////////////
Text _.autodoc.member.Ortogonalization ="Fun��o de Ortogonaliza��o de S�ries "
"Dado um conjunto de outputs, retorna a ortogonaliza��o em rela��o aos outputs";
Serie Ortogonalization(Serie output, Set inputs_set)
//////////////////////////////////////////////////////////////////////////////
{
	Set Result_Detect = Identify(Serie output, Set inputs_set, Real 1, Real 1, Set Empty);
	WriteLn(" ");
	WriteLn("ARIMA: " << Result_Detect[1]);
	Serie Result_Detect[3][3]
};

//////////////////////////////////////////////////////////////////////////////
// Fun��o de Gera��o de Ciclos Sazonais
// Dado um TimeSet e uma data inicial, e uma periodicidade, gera um ciclo
// sazonal determin�stico de soma zero
//////////////////////////////////////////////////////////////////////////////

Set _SeasonalCycle(Real Period, TimeSet fec_output, Date ini)
{

	Set Cycle = For(1, (Period-1), Serie (Real conta) {
		Eval("TimeSet Cycle"+FormatReal(conta)+" = Periodic(Succ(ini, fec_output, "+FormatReal(conta-1)+"), Period, fec_output);
		TimeSet CycleP = Periodic(Succ(ini, fec_output, (Period-1)), Period, fec_output);
		Serie K_"+FormatReal(conta)+"_"+FormatReal(Period)+" = CalInd(Cycle"+FormatReal(conta)+",fec_output)-CalInd(CycleP,fec_output)")
	});

	Set cycleInputSet = For(1, (Period-1), Set (Real conta) {
		InputDef(Polyn 1, Serie Cycle[conta])
	})
};


//////////////////////////////////////////////////////////////////////////////
Text _.autodoc.member.RemoveSeasonality ="Fun��o de Processamento de Sinais "
" - Remove a sazonalidade (anual) dos dados, capturando a tend�ncia "
"Dado um conjunto de outputs, retorna a ortogonaliza��o em rela��o aos outputs";
Serie RemoveSeasonality(Serie output, Real Period){
//////////////////////////////////////////////////////////////////////////////


	TimeSet fec_output = Dating(output);

	Set inputs_set = _SeasonalCycle(Period, fec_output, First(output));
		
	Set Result_Detect = Identify(Serie output, Set inputs_set, Real 1, Real 1, Set Empty);

	WriteLn(" ");
	WriteLn("ARIMA: " << Result_Detect[1]);

	Serie Result_Detect[3][3]
};

//////////////////////////////////////////////////////////////////////////////
Text _.autodoc.member.GetSeasonality ="Fun��o de Processamento de Sinais "
" - Captura a sazonalidade (dada pelo Period) dos dados, capturando a tend�ncia";
Serie GetSeasonality(Serie output, Real Period){
//////////////////////////////////////////////////////////////////////////////

	TimeSet fec_output = Dating(output);

	Set inputs_set = _SeasonalCycle(Period, fec_output, First(output));
	
	Set Result_Detect = Identify(Serie output, Set inputs_set, Real 1, Real 1, Set Empty);

	WriteLn(" ");
	WriteLn("ARIMA: " << Result_Detect[1]);

	Serie Filtro_Resultado = Result_Detect[3][8]
};

//////////////////////////////////////////////////////////////////////////////
Text _.autodoc.member.ForecastError ="Fun��o de C�lculo do Erro Te�rico da Previs�o - "
" A id�ia basicamente consiste em avaliar o efeito impulso-resposta da estrutura ARIMA "
" como um sistema din�mico, e somar os efeitos propagados, e ponderar pelo sigma dos "
" res�duos. Retorna a s�rie de erros te�ricos";
Serie ForecastError(Ratio ARIMA, Serie Residuals, Real p, Date ini) {
//////////////////////////////////////////////////////////////////////////////

	TimeSet OutFec = Dating(Residuals);
	Real SigmaRes = StDsS(Residuals);

	// Ratio ARIMA = ((1-0.57*B)/(1-B)); Real SigmaRes = 0.05;  TimeSet OutFec = Mensual;
	// Real p = 5; Date ini = y2005m01;

	// Faz uma expans�o da raz�o polinomial em uma soma finita

	Polyn psi = Expand(ARIMA, p);
	
	Serie sigma2 = SigmaRes^2;
	
	Date Last = Succ(ini, OutFec, p);

	// Aplica a soma em uma s�rie Pulso

	Serie Expansion = SubSer(psi:Pulse(ini, OutFec), ini, Last);

	Serie Expansion_Sqr = Expansion^2;

	// Faz o somat�rio dos efeitos ao quadrado
	
	Serie Propagation = DifEq(1/(1-B), Expansion_Sqr,0);

	// Pondera pelo sigma2 dos res�duos em estima��o

	Serie Error2 = sigma2*Propagation;

	Serie Error = SubSer(Sqrt(Error2), ini, Last(Error2))	
	
};

//////////////////////////////////////////////////////////////////////////////
Text _.autodoc.member.BackForecast = " Fun��o de Backforecast de uma s�rie - "
" Dada uma sazonalidade clara, faz um backforecast para p per�odos e devolve "
" a s�rie total";
Serie BackForecast (Serie output, Set inputs_set, Set Periods){
//////////////////////////////////////////////////////////////////////////////

/* A id�ia basicamente consiste em inverter uma s�rie, partindo do pressuposto
 de estacionariedade, ou seja, tanto faz prever uma s�rie em fun��o de seu
 sentido cronol�gico, e estimar ela em fun��o da s�rie a qual ela seria o output,
 para ter coer�ncia em rela��o a tend�ncia e outros efeitos, de maneira a
 detectar o melhor ARIMA para realizar a tarefa proposta.

 Posteriormente, anexar o backforecast a s�rie original */

	Serie inv_out = ReverseSer(output);

	Serie backserie = Identify(inv_out, inputs_set, Real 1, Real 1, Periods)[3][7];

	Serie ori_output = ReverseSer(backserie);

	Serie complete_serie = ori_output << output

}

]];
