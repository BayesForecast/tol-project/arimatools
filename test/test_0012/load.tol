#Require ArimaTools;

Real rndSeed = 0;
Real PutRandomSeed(rndSeed);
Real rndSeed := GetRandomSeed(0);

Real ArimaTools::@IdArmaDB::Open(?);

Real maxLag = 20;
Real useLag = 10;
Real subSmpSiz = 10000;

Set dbSamplerStats = DBTable("select * from model_f_acf_20_stats")[1];

Real smpSiz = dbSamplerStats::nu_sample_size;
Real pMin = dbSamplerStats::nu_p_min;
Real pMax = dbSamplerStats::nu_p_max;
Real qMin = dbSamplerStats::nu_q_min;
Real qMax = dbSamplerStats::nu_q_max;
Real Nmin = dbSamplerStats::nu_data_min;
Real Nmax = dbSamplerStats::nu_data_max;

Real p_ = IntRand(pMin,pMax);
Real q_ = IntRand(qMin,pMax);

Real Show(False,"ALL");
Set Include("rndArima.tol");
Set Include("../generateArima.tol");
Real Show(True,"ALL");

Real numLag = Min(useLag,maxLag,Rows(acor));
Matrix acf = Sub(acor,1,1,numLag,1);
Matrix pacf = Sub(pacor,2,1,numLag-1,1);

Matrix tacov = ARMATACov(ar, ma, Rows(acor)+1);
Matrix tacor = tacov*1/MatDat(tacov,1,1);
Matrix tacf  = Sub(tacor,2,1,numLag,1);
Matrix acf.cmp.real = acf | tacf;
  

Matrix acf.cov  = ARMA.ACF.Bartlett.Cov(acor,  numLag, N);
Matrix tacf.cov = ARMA.ACF.Bartlett.Cov(tacor, numLag, N);

Matrix tacf.cov.L = Choleski(tacf.cov);

Matrix acf.dif = (acf - tacf);
Matrix acf.dif.01 = LTSolve(tacf.cov.L,acf.dif);


Set arima;

Real acf_1_h = .05;
Real acf_1_min = Round((MatDat(acf,1,1) - acf_1_h)*1E4)/1E4;
Real acf_1_max = Round((MatDat(acf,1,1) + acf_1_h)*1E4)/1E4;
Real acf_2_h = .1;
Real acf_2_min = Round((MatDat(acf,2,1) - acf_2_h)*1E4)/1E4;
Real acf_2_max = Round((MatDat(acf,2,1) + acf_2_h)*1E4)/1E4;
Real pacf_2_h = .1;
Real pacf_2_min = Round((MatDat(pacf,2,1) - pacf_2_h)*1E4)/1E4;
Real pacf_2_max = Round((MatDat(pacf,2,1) + pacf_2_h)*1E4)/1E4;

Real get_acf_count(Real void)
{
  Text query = "SELECT count(*) FROM model_f_acf_"<<maxLag+" "+
  "WHERE "
  "  acf_1 >= "<< acf_1_min+" AND  acf_1 <= "<< acf_1_max+" \n AND"
  "  acf_2 >= "<< acf_2_min+" AND  acf_2 <= "<< acf_2_max+" \n AND"
  " pacf_2 >= "<<pacf_2_min+" AND pacf_2 <= "<<pacf_2_max+" \n";
//WriteLn("TRACE [get_acf_count] query= \n"+query+"\n");
  Real t0=Copy(Time);
  Real c = DBTable(query)[1][1];
  Real tm=Copy(Time)-t0;
//WriteLn("TRACE [get_acf_count] query elapsed time: "<<tm+" s.");
  c
};

Real acf_count = 
{
  Real acf_count_a1a2p2 = get_acf_count(?);
  Real acf_count_a1a2 = If(acf_count_a1a2p2>=subSmpSiz,acf_count_a1a2p2,
  {
    Real pacf_2_min := -1;
    Real pacf_2_max := +1;
    get_acf_count(?)
  });
  If(acf_count_a1a2>=subSmpSiz,acf_count_a1a2,
  {
    Real acf_2_min := -1;
    Real acf_2_max := +1;
    get_acf_count(?)
  })
};

WriteLn("TRACE [get_acf_count] acf_count="<<acf_count);

Real N_h = 0.5*subSmpSiz/acf_count;
Real nu_data_min = Floor(N - N_h*(Nmax-Nmin));
Real nu_data_max = Ceil(N + N_h*(Nmax-Nmin));

Set neighbours = {
  Text query_arma ="\n"+
  "SELECT * FROM (SELECT \n"+
  "  id_model, te_ar, te_ma, \n"+
  "Sqrt("+SetSum(For(1,pMax+qMax,Text(Real k)
  {
  If(k>1," +","  ")+
  "power(acf_"<<k<<FormatReal(MatDat(acf, k,1),"%+.7lf")+",2)\n"
  }))+
  SetSum(For(2,pMax+qMax,Text(Real k)
  {
  " +power(pacf_"<<k<<FormatReal(MatDat(pacf, k-1,1),"%+.7lf")+",2)\n"
  }))+") as distance \n"+
  "FROM model_f_acf_"<<maxLag+" \n"
  "WHERE \n"+
  "    nu_data >="<<nu_data_min+" \n"+
  "  AND nu_data <="<<nu_data_max+" \n"+
  "  AND acf_1 >= "<<acf_1_min+" \n"+
  "  AND acf_1 <= "<<acf_1_max+" \n"+
  "  AND acf_2 >= "<<acf_2_min+" \n"+
  "  AND acf_2 <= "<<acf_2_max+" \n"+
  "  AND pacf_2 >= "<<pacf_2_min+" \n"+
  "  AND pacf_2 <= "<<pacf_2_max+" \n"+
  ") AS AUX1 ORDER BY distance\n"+
  "LIMIT "<<subSmpSiz+"; \n";

//WriteLn("TRACE [neighbours] query_arma= \n"+query_arma+"\n");

  Real t0=Copy(Time);
  Set aux.0 = DBTable(query_arma);
  Real t1=Copy(Time);
//WriteLn("TRACE [neighbours] query elapsed time: "<<Real (t1-t0)+" s.");

  Set aux.1 = EvalSet(aux.0,Set(Set s)
  {[[
    Real s::id_model,
    Polyn phi = Eval(s::te_ar),
    Polyn theta = Eval(s::te_ma),
    Real p = Degree(phi),
    Real q = Degree(theta),
    Real s::distance
  ]]});
  Real t2=Copy(Time);
//WriteLn("TRACE [neighbours] eval polynomials elapsed time: "<<Real (t2-t1)+" s.");
  
  Set aux.2 = EvalSet(aux.1,Set(Set s)
  {
    Real llh = ARMA.ACF.Bartlett.LLH(s::phi, s::theta, acf, N)
        - ArimaTools::UnitRoot::LogVRegion(s::p)
        - ArimaTools::UnitRoot::LogVRegion(s::q);
    s<<[[llh]]
  });
  Real t3=Copy(Time);
//WriteLn("TRACE [neighbours] eval likelihood elapsed time: "<<Real (t3-t2)+" s.");
  Set aux.3 = Sort(aux.2,Real(Set a, Set b)
  {
    Compare(b::llh,a::llh)
  })
};

Set ranking = 
{
  Real t0=Copy(Time);
  Real SetIndexByName(neighbours[1]);
  Real llh.pos = FindIndexByName(neighbours[1],"llh");
  Set pqClass = {
    Set aux = Classify(neighbours,Real(Set a, Set b)
    {
      Compare(a::p,b::p)*10+
      Compare(a::q,b::q)
    });
    Set aux2 = EvalSet(aux,Set(Set cls)
    {
      Real p = (cls[1])::p;
      Real q = (cls[1])::q;
      Set  s = Sort(cls,Real(Set a, Set b)
      {
        Compare(b::llh,a::llh)
      });
      Eval("p"<<p+"q"<<q+"=s")
    });
    Sort(aux2,Real(Set a, Set b)
    {
      Compare(Card(b),Card(a))
    })
  };
  Real t1=Copy(Time);
//WriteLn("TRACE [ranking] classify elapsed time: "<<Real (t1-t0)+" s.");
  Set aux.1 = EvalSet(pqClass,Set(Set cls)
  {
    Real card = Card(cls);
    Matrix llh = SetCol(Traspose(Extract(cls,llh.pos))[1]);
    Real llhMax = MatMax(llh);
    Matrix weight = Exp(llh-llhMax);
    Real sumWeight = MatSum(weight);
    Real logWeight = Log(MatSum(weight))+llhMax;
    Polyn phi = SetSum(EvalSet(cls,Polyn(Set a)
    {
      (a::phi)*Exp(a::llh-llhMax)
    })) / sumWeight;
    Polyn theta = SetSum(EvalSet(cls,Polyn(Set a)
    {
      (a::theta)*Exp(a::llh-llhMax)
    })) / sumWeight;
    Real p = (cls[1])::p;
    Real q = (cls[1])::q;
    Real prob = ?;
    Matrix acf.cmp = {
      Matrix tacov = ARMATACov(phi, theta, numLag+1);
      acf.cmp.real |
      Sub(tacov,2,1,numLag,1)*1/MatDat(tacov,1,1) 
    };
    [[
      card, logWeight, prob, p, q, phi, theta, acf.cmp, cls
    ]]
  });
  Real t2=Copy(Time);
//WriteLn("TRACE [ranking] identifying elapsed time: "<<Real (t2-t1)+" s.");
  Matrix logWeight = SetCol(Traspose(Extract(aux.1,2))[1]);
  Real maxLogWeight = MatMax(logWeight);
  Matrix weight = Exp(logWeight-maxLogWeight);
  Real sumWeight = MatSum(weight);
  Matrix prob = weight * 1/sumWeight;
  Set For(1,Card(aux.1),Real(Real k) { (aux.1[k])::prob:=MatDat(prob,k,1) });
  Set aux.2 = Sort(aux.1,Real(Set a, Set b)
  {
    Compare(b::logWeight, a::logWeight)
  })
};

Set View(arima[1],"Std");WriteLn("");

/* */
 
