//////////////////////////////////////////////////////////////////////////////
// FILE   : Identify.tol
// PURPOSE: Classes for ARIMA identification by means of multimodel sampling
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
Class @PolDeg.Sampler
//////////////////////////////////////////////////////////////////////////////
{
  Static NameBlock Options = 
  [[
     NameBlock CandidateGen = [[
       Real MoveAll = 1;
       Real MoveOne = 2;
       Real MoveOneForced = 3
     ]]
  ]];

  Real _.min;
  Real _.max;

  Set _.transition = Copy(Empty);
  Set _.transition.log = Copy(Empty);

  Real lambda = 0.5;

  ////////////////////////////////////////////////////////////////////////////
  Real build_transition(Real lambda_, Real candidateGen) 
  ////////////////////////////////////////////////////////////////////////////
  { 
    Real lambda := lambda_;
    
    Set _.transition := For(_.min, _.max, Set(Real x)
    {
      Set aux = For(_.min, _.max, Real(Real y)
      {
        Case(
          x!=y, lambda^Abs(x-y),
          candidateGen==ArimaTools::@PolDeg.Sampler::Options::
          CandidateGen::MoveOneForced, 0,
          1==1, 1)
      });
      Real sum = SetSum(aux);
      For(1,Card(aux),Real(Real n) { aux[n]/sum } )
    });
    Set _.transition.log := For(1, Card(_.transition), Matrix(Real k)
    {
      Log(SetCol(_.transition[k]))
    });
    True 
  };

  ////////////////////////////////////////////////////////////////////////////
  Real Q.draw(Real x)
  ////////////////////////////////////////////////////////////////////////////
  {
    Real k = x+1-_.min;
    Matrix y = BysSampler::CppTools::RandPropLogWeight(_.transition.log[k],1);
    MatDat(y,1,1)+_.min-1
  };

  ////////////////////////////////////////////////////////////////////////////
  Real Q.logDens(Real y, Real x)
  ////////////////////////////////////////////////////////////////////////////
  {
    MatDat(_.transition.log[x+1-_.min],y+1-_.min,1)
  }
};

//////////////////////////////////////////////////////////////////////////////
Class @FactorDegreeSampler
//////////////////////////////////////////////////////////////////////////////
{
  Real period;
  @PolDeg.Sampler ar;
  @PolDeg.Sampler ma;
  @PolDeg.Sampler dif;

  ////////////////////////////////////////////////////////////////////////////
  Real build_transition(Real lambda_, Real candidateGen) 
  ////////////////////////////////////////////////////////////////////////////
  {
    Real ar::build_transition(lambda_, candidateGen);
    Real ma::build_transition(lambda_, candidateGen);
    Real dif::build_transition(lambda_, candidateGen);
    True 
  }
};

//////////////////////////////////////////////////////////////////////////////
Class @ARIMA.Sampler : @MHSampler
//////////////////////////////////////////////////////////////////////////////
{
  @Matrix _.noise;
  //Set of @ARIMADegreeRangeStruct
  Set _.degreeRange; 
  //Set of @FactorDegreeSampler
  Set _.degreeSampler = Copy(Empty);
  //Set of real
  Set _.period = Copy(Empty);
  //Set of references to each degree samplers
  Set _.degRef = Copy(Empty);
  //Set of @ARMA.Splitter.ACF
  Set _.FactorACF = Copy(Empty);
  //Set of @ARMA.Sampler
  Set _.armaSampler = Copy(Empty);
  Real _.factorNum = ?;
  Set _.varMin = Copy(Empty);
  Set _.varMax = Copy(Empty);
  Set _.varFree = Copy(Empty);

  Set _.histogram = Copy(Empty);
  Set _.ranking = Copy(Empty);
  Matrix _.mcmc.id = Constant(0,0,?);

  NameBlock config = [[Real _.undefined=? ]];

  ////////////////////////////////////////////////////////////////////////////
  Real setDefaultConfig(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    NameBlock config :=
    [[
       Real Evaluator = 
         ArimaTools::@ARMA.Sampler::Options::Evaluator::Levinson;
       Real CandidateGen =
         ArimaTools::@PolDeg.Sampler::Options::CandidateGen::MoveAll;
       Real InitialOptNum = 0;
       Real InitialDrawNum = 0
    ]];
    True
  };

  ////////////////////////////////////////////////////////////////////////////
  Static @ARIMA.Sampler CreateFromDegreeRange(Matrix noise, Set degreeRange)
  ////////////////////////////////////////////////////////////////////////////
  {
    @ARIMA.Sampler arimaSampler = 
    [[
      @Matrix _.noise = [[noise]];
      Set _.degreeRange = DeepCopy(degreeRange)
    ]];
    Real arimaSampler::initialize(?);
    arimaSampler
  };

  ////////////////////////////////////////////////////////////////////////////
  Real setEvalMethod(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    True
  };

  ////////////////////////////////////////////////////////////////////////////
  Matrix get.initialPoint(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    SetCol(_.varMin)
  };

  ////////////////////////////////////////////////////////////////////////////
  Real initialize(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    Real setDefaultConfig(?);
    Real _.factorNum := Card(_.degreeRange);
    Set _.period := EvalSet(_.degreeRange, Real(@ARIMADegreeRangeStruct dr)
    {
      dr->Periodicity
    });
    Set _.degreeSampler := EvalSet(_.degreeRange, NameBlock(@ARIMADegreeRangeStruct dr)
    {
      @FactorDegreeSampler fds = [[
        Real period = dr->Periodicity;
        @PolDeg.Sampler ar = [[
          Real _.min = dr->MinARDegree,
          Real _.max = dr->MaxARDegree ]];
        @PolDeg.Sampler ma = [[
          Real _.min = dr->MinMADegree,
          Real _.max = dr->MaxMADegree ]];
        @PolDeg.Sampler dif = [[
          Real _.min = dr->MinDIFDegree,
          Real _.max = dr->MaxDIFDegree ]]
      ]];

      Set Append(_.degRef, [[fds::ar,fds::ma,fds::dif]]);
      fds
    });
    Set _.varName := SetConcat(For(1, _.factorNum, Set(Real k)
    {
      Text prfx = "F."<<k+"_S."<<_.degreeRange[k]->Periodicity+"_";
      SetOfText(prfx+"AR", prfx+"MA", prfx+"DIF")
    }));
    Set _.varMin := SetConcat(EvalSet(_.degreeRange, Set(@ARIMADegreeRangeStruct dr)
    {
      SetOfReal(dr->MinARDegree, dr->MinMADegree, dr->MinDIFDegree)
    }));
    Set _.varMax := SetConcat(EvalSet(_.degreeRange, Set(@ARIMADegreeRangeStruct dr)
    {
      SetOfReal(dr->MaxARDegree, dr->MaxMADegree, dr->MaxDIFDegree)
    }));
    Set _.varFree := Select(Range(1,Card(_.varMin),1),Real(Real k)
    {
      _.varMin[k]<_.varMax[k]
    });
    Real _.varNum := Card(_.varName);
    Real stepSize := 0.5;
    Real tunning.acceptTarget := 0.50;
    Real tunning.sMax := 1;
    Real tunning.sMin := 0;
    Real tunning.lapse := 10;
    Real postTunning(?); 
    True
  };

  ////////////////////////////////////////////////////////////////////////////
  Real postTunning(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    Set EvalSet(_.degreeSampler,Real(@FactorDegreeSampler fds)
    {
      fds::build_transition(stepSize,config::CandidateGen)
    });
    True 
  };

  ////////////////////////////////////////////////////////////////////////////
    Text modelID(Matrix deg)
  ////////////////////////////////////////////////////////////////////////////
  {
    ToName(Compact(Replace(ToName("MID_"<<deg),"_"," ")))
  };

  ////////////////////////////////////////////////////////////////////////////
    Text difID(Matrix deg)
  ////////////////////////////////////////////////////////////////////////////
  {
    "DID"+SetSum(For(1,_.factorNum,Text(Real f)
    {
      "_"<<MatDat(deg,f*3,1)
    }))
  };

  ////////////////////////////////////////////////////////////////////////////
    @NameBlock storedDif(Matrix deg)
  ////////////////////////////////////////////////////////////////////////////
  {
    Text id = difID(deg);
    Real idx = FindIndexByName(_.FactorACF,id);
    If(idx, @NameBlock(_.FactorACF[idx]),
    {
      Real c = Card(_.armaSampler);
      Polyn dif = SetProd(For(1,_.factorNum,Polyn(Real f)
      {
        Real d = MatDat(deg,f*3,1);
        (1-B^_.period[f])^d
      }));
      Set ref = @NameBlock(@ARMA.Splitter.ACF::CreateFromDegRng(
        $_.noise, dif, _.degreeRange));
      NameBlock PutName(id,$ref);
      Set Append(_.FactorACF, ref);
      ref
    })
  };
      
  ////////////////////////////////////////////////////////////////////////////
    @NameBlock storedModel(Matrix deg)
  ////////////////////////////////////////////////////////////////////////////
  {
    Text id = modelID(deg);
    Real idx = FindIndexByName(_.armaSampler,id);
    If(idx, @NameBlock(_.armaSampler[idx]),
    {
      Real c = Card(_.armaSampler);
      Set ref = @NameBlock(@ARMA.Sampler::CreateFromDegVec(
        $_.noise, _.period, deg));
      Real $ref::config::Evaluator := config::Evaluator;

      Real $ref::setEvalMethod(?);
      Set $ref::_.factor.acf := storedDif(deg);
      Real $ref::set.initialPoint($ref::get.initialPoint(?));
      Real If(config::InitialOptNum,
      {
        Real $ref::optimize(?);
        True
      });
/* */
      Set For(1,config::InitialDrawNum,Real(Real iter)
      {
        Real llh = $ref::pi.logDens.sttCumAvr.draw(?)
      });
/* */
      NameBlock PutName(id,$ref);
      Set Append(_.armaSampler, ref);
      ref
    })
  };

  ////////////////////////////////////////////////////////////////////////////
  Matrix Q.draw.moveAll(Matrix x)
  ////////////////////////////////////////////////////////////////////////////
  {
    SetCol(For(1,_.varNum,Real(Real k)
    {
      If(_.varMin[k]==_.varMax[k], _.varMin[k],
      {
        @NameBlock dr = [[ _.degRef[k] ]];
        Real m = MatDat(x,k,1);
        $dr::Q.draw(m)
      })
    }))
  };

  ////////////////////////////////////////////////////////////////////////////
  Real Q.logDens.moveAll(Matrix y, Matrix x)
  ////////////////////////////////////////////////////////////////////////////
  {
    SetSum(For(1,_.varNum,Real(Real k)
    {
      Real m = MatDat(x,k,1);
      Real n = MatDat(y,k,1);
      If(_.varMin[k]==_.varMax[k], If(m==n, 0, -1/0),
      {
        @NameBlock dr = [[ _.degRef[k] ]];
        $dr::Q.logDens(n,m)
      })
    }))
  };

  Real _.lastMoving = 1;

  ////////////////////////////////////////////////////////////////////////////
  Matrix Q.draw.moveOne(Matrix x)
  ////////////////////////////////////////////////////////////////////////////
  {
    Real vf = IntRand(1,Card(_.varFree));
    Real k = _.varFree[vf];
    Real _.lastMoving := k;
  //WriteLn("TRACE Q.draw.moveOne _.lastMoving = "<<_.lastMoving);
    @NameBlock dr = [[ _.degRef[k] ]];
    Real m = MatDat(x,k,1);
    Real yk = $dr::Q.draw(m);
    Matrix y = x+0;
    Real PutMatDat(y,k,1,yk);
    y
  };

  ////////////////////////////////////////////////////////////////////////////
  Real Q.logDens.moveOne(Matrix y, Matrix x)
  ////////////////////////////////////////////////////////////////////////////
  {
  //WriteLn("TRACE Q.logDens.moveOne _.lastMoving = "<<_.lastMoving);
    Real k = _.lastMoving;
    @NameBlock dr = [[ _.degRef[k] ]];
    Real m = MatDat(x,k,1);
    Real n = MatDat(y,k,1);
    $dr::Q.logDens(n,m)
  };

  ////////////////////////////////////////////////////////////////////////////
  Matrix Q.draw(Matrix x)
  ////////////////////////////////////////////////////////////////////////////
  {
    If(config::CandidateGen==
       ArimaTools::@PolDeg.Sampler::Options::CandidateGen::MoveAll,
       Q.draw.moveAll(x),
       Q.draw.moveOne(x))
  };

  ////////////////////////////////////////////////////////////////////////////
  Real Q.logDens(Matrix y, Matrix x)
  ////////////////////////////////////////////////////////////////////////////
  {
    If(config::CandidateGen==
       ArimaTools::@PolDeg.Sampler::Options::CandidateGen::MoveAll,
       Q.logDens.moveAll(y,x),
       Q.logDens.moveOne(y,x))
  };

  ////////////////////////////////////////////////////////////////////////////
  NameBlock reversible_jump(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {[[
    Matrix y = Q.draw(_.x);
    @NameBlock y.arma = storedModel(y);
    @NameBlock x.arma = storedModel(_.x);
    Real y.model.prior = $y.arma::_.priorLogDens;
    Real x.model.prior = $x.arma::_.priorLogDens;
    Real y.beta.Q.logDens = $y.arma::_.stationaryLogDens;
    Real x.beta.Q.logDens = $x.arma::_.stationaryLogDens;
    Real llh.x = x.model.prior + pi.logDens.last(_.x);
    Real llh.y = y.model.prior + pi.logDens(y);
    Real qld.xy = Q.logDens(_.x,   y) + x.beta.Q.logDens;
    Real qld.yx = Q.logDens(  y, _.x) + y.beta.Q.logDens;
    Real a = Exp(llh.y + qld.xy - llh.x - qld.yx)
  ]]};

  ////////////////////////////////////////////////////////////////////////////
  Real pi.logDens(Matrix y)
  ////////////////////////////////////////////////////////////////////////////
  {
    @NameBlock arma = storedModel(y);
    Real llh = $arma::pi.logDens.sttCumAvr.draw(?);
    llh
  };

/* * /
  ////////////////////////////////////////////////////////////////////////////
  Real pi.logDens.last(Matrix x)
  ////////////////////////////////////////////////////////////////////////////
  {
    pi.logDens(x)
  };
/* */

  ////////////////////////////////////////////////////////////////////////////
  Real postDraw(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    True
  };

  ////////////////////////////////////////////////////////////////////////////
  Real build_mcmc(Real sampleLength)
  ////////////////////////////////////////////////////////////////////////////
  {
    Real _build_mcmc(sampleLength);
    Real _build_sample(?);
    Set _.histogram := For(1, _.varNum, Matrix(Real k)
    {
      Real min = _.varMin[k];
      Real max = _.varMax[k];
      Matrix freq = SetMat(For(min,max,Set(Real i)
      {
        Real f = MatAvr(EQ(_.sample[k],i));
        [[i,f]]
      }));
      Eval("Histogram."+_.varName[k]+"=freq")
    });
    Real burnin = Floor(sampleLength/2);
    Real length.afterBurn = sampleLength-burnin;
    Set idx = Range(1,sampleLength,1);
    Set idx.afterBurn = Range(burnin+1,sampleLength,1);
    Set id = EvalSet(idx,Text(Real k)
    {
      modelID(Tra(SubRow(_.mcmc,[[k]])))
    });
    Matrix _.mcmc.id := SetCol(EvalSet(id, Real(Text idk)
    {
      FindIndexByName(_.armaSampler, idk)
    }));
    Set ranking.aux1 = 
    {
      Set cls = Classify(idx.afterBurn,Real(Real a, Real b)
      {
        Compare(id[a],id[b])
      });
      Real weight.cum = 0;
//    Real weight.cum = length.afterBurn;
      Matrix sc = SetMat(EvalSet(cls,Set(Set c)
      {
        Real row = c[1];
        Real modNum = MatDat(_.mcmc.id,row,1);
        Real llh.avr = (_.armaSampler[modNum])::_.llh.avr;
        Real weight = Exp(llh.avr-_.llh.max);
      //Real weight = Card(c);
        Real If(IsUnknown(weight), weight := 0);
        Real weight.cum := weight.cum + weight;
        [[weight, row, modNum]]
      }));
      Matrix scs = Sort(sc,SetOfReal(-1));
      Matrix scsp = PivotByRows(sc,scs);
      For(1,Rows(scsp),Set(Real k)
      {
        Real weight = MatDat(scsp,k,1);
        Real row    = MatDat(scsp,k,2);
        Real modNum = MatDat(scsp,k,3);
        Real prob = weight/weight.cum;
        Text name = Name(_.armaSampler[modNum]);
        Text label = (_.armaSampler[modNum])::_.label;
        [[prob]]<<For(1,_.varNum,Real(Real j)
        {
          Real deg = MatDat(_.mcmc,row,j);
          Eval(_.varName[j]+"=deg")
        })<<[[ name, label ]]
      })
    };
    Set _.ranking := ranking.aux1;
    True
  };

  ////////////////////////////////////////////////////////////////////////////
  Real clear(Real void)
  ////////////////////////////////////////////////////////////////////////////
  { 
    Set _.noise := Copy(Empty); 
    True
  };

  ////////////////////////////////////////////////////////////////////////////
  Real __destroy(Real void)
  ////////////////////////////////////////////////////////////////////////////
  { 
    clear(void)
  }

};


/* */
