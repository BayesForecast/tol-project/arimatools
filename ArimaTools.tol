//////////////////////////////////////////////////////////////////////////////
// FILE   : ArimaTools.tol
// PURPOSE: Tools related to ARIMA models.
//////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////
NameBlock ArimaTools =
//////////////////////////////////////////////////////////////////////////////
[[
#Require NonLinGloOpt;
#Require BysSampler;

//read only autodoc
Text _.autodoc.name = "ArimaTools";
Text _.autodoc.brief =
"Tools related to ARIMA and ARMA models.";
Text _.autodoc.description = "Tools related to ARIMA and ARMA models."
"More details in "
"<a href=\"https://www.tol-project.org/wiki/"
"OfficialTolArchiveNetworkArimaTools\" target=\"_blank\"> "
"wiki page</a> of ArimaTools package.";
Text _.autodoc.url = 
"http://packages.tol-project.org/OfficialTolArchiveNetwork/repository.php";
Set _.autodoc.keys = [["ARIMA","Generator","Identification","Units roots","ARMA"]];
Set _.autodoc.authors = [[ "vdebuen@tol-project.org"]];
Text _.autodoc.minTolVersion = "v3.2 b016";
Real _.autodoc.version.high = 7;
Real _.autodoc.version.low = 2;
Set _.autodoc.dependencies = Copy(Empty);
Set _.autodoc.nonTolResources = [[
  Text "data"
]] ;
Text _.autodoc.versionControl = AvoidErr.NonDecAct(OSSvnInfo("."));

//private implementation
#Embed "RandArima.tol";
#Embed "IdArmaDB.tol";
#Embed "arma_process.tol";
#Embed "OLS.tol";
#Embed "Filter.tol";
#Embed "UnitRoot.tol";
#Embed "Sampler.tol";
#Embed "ArimaVector.tol";
#Embed "ARMA.Sampler.tol";
#Embed "ARMA.Sampler.ACF.Reg.tol";
#Embed "IdArmaSearch.tol";
#Embed "ARMA.Splitter.ACF.tol";

#Embed "Identify.Mcmc.tol";
#Embed "GenArima/GenArima.tol";
#Embed "Swartz.tol";
#Embed "ARIMA.Identifier.tol";
#Embed "ExpSmooth.tol";
#Embed "SearchAcf.tol";
#Embed "FillXArima.tol";

NameBlock SearchAcf = [[Real _unused=?]];

Text _packageRoot = ".";
Real _is_started = False;

////////////////////////////////////////////////////////////////////////////
Real StartActions(Real void)
////////////////////////////////////////////////////////////////////////////
{
  If(_is_started, False, {
//WriteLn("TRACE [ARIMATools::StartActions]");
  Real _is_started := True;
  Set DiagnosticsBounds := 
    DeepCopy(ArimaTools::ARIMA.Identifier::DiagBounds);
  Text _packageRoot := GetAbsolutePath("./");
  NameBlock SearchAcf := BuildSearchAcf(?);
  True
})}

]];
  











