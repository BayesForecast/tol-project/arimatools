//////////////////////////////////////////////////////////////////////////////
// FILE   : ARMA.Sampler.ACF.Reg
// PURPOSE: Class for sampling regular ARMA models based on ACF approximation
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
Class @ARMA.Sampler.ACF.Reg.Config
//////////////////////////////////////////////////////////////////////////////
{
  ////////////////////////////////////////////////////////////////////////////
  Static NameBlock Options = 
  ////////////////////////////////////////////////////////////////////////////
  [[
     NameBlock ACFCov = [[
       Text CommutedBarlett   = "CommutedBarlett";
       Text TheoricalBartlett = "TheoricalBartlett"
     ]];
     NameBlock PriorType = [[
       Text NonInf = "NonInf";
       Text InvStatVol = "InvStatVol";
       Text BIC = "BIC";
       Text AIC = "AIC";
       Text AICc = "AICc"
     ]];
     NameBlock Sampler = [[
       Text PriorSampling      = "PriorSampling";
       Text ImportanceSampling = "ImportanceSampling";
       Text MetropolisHastings = "MetropolisHastings"
     ]]
  ]];
  Real acfContrastCoef = 1;
  Real acfUseLag = 10;
  Real acfSkipSatellites = 0;
  Real usePreliminarEstimation = True;
  Text acfCov = @ARMA.Sampler.ACF.Reg.Config::Options::ACFCov::
                CommutedBarlett;
  Text priorType = @ARMA.Sampler.ACF.Reg.Config::Options::PriorType::
                   InvStatVol;
  Text sampler = @ARMA.Sampler.ACF.Reg.Config::Options::Sampler::
                 ImportanceSampling;

  Text optMethod = "LD_VAR2";
  Real optIter = 0;
  Real optMaxTime = 2;
  Real optAbsoluteTolerance_target = 1E-8;
  Real optRelativeTolerance_target = 1E-8;
  Real optRelativeTolerance_var = 1E-8;
  Real optVerbose = False;

  Real burninRatio = 0.5; 
  Real sampleLength = 1000;

  ////////////////////////////////////////////////////////////////////////////
  Static @ARMA.Sampler.ACF.Reg.Config Default(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    @ARMA.Sampler.ACF.Reg.Config default = [[
      Real OptIter = 0
    ]]
  }
};

//////////////////////////////////////////////////////////////////////////////
Class @ARMA.Sampler.ACF.Reg : @ArimaVector, @MHSampler
//////////////////////////////////////////////////////////////////////////////
{
  Real _.s;
  Real _.N;
  Matrix _.acor_full;
  Matrix _.acor = Constant(0,0,?);
  Real _.p;
  Real _.q;
  Real _.K = ?;
  Set _.sel = Copy(Empty);
  Real _.Ks = ?;
  Matrix _.samplingBartlett.cov = Constant(0,0,?);
  Matrix _.samplingBartlett.cov_L = Constant(0,0,?);
  Real _.samplingBartlett.exact = ?;
  Matrix _.mle = Constant(0,0,?);
  Real _.logDetL = 0; 
  Real _.priorLogDens = 0;
  Text _MID = "[@ARMA.Sampler.ACF.Reg]";
  Set config = Copy(Empty);
  Real _lastVerbose = True;

  Matrix _.asymtotic.x = Constant(0,0,?);
  Matrix _.asymtotic.S.Lt = Constant(0,0,?);
  Real _.asymtotic.S.logDet = ?;

  ////////////////////////////////////////////////////////////////////////////
  Real setDefaultConfig(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    Set config := @NameBlock(@ARMA.Sampler.ACF.Reg.Config::Default(?));
    True
  };

  ////////////////////////////////////////////////////////////////////////////
  Matrix SafeCholesky(Matrix cov)
  ////////////////////////////////////////////////////////////////////////////
  {
    Real vrb = _lastVerbose;
    Real _lastVerbose := False;
    Real Show(_lastVerbose,"ALL");
    Real nErr0 = Copy(NError);
    Real nWar0 = Copy(NWarning);
    Matrix  L = Choleski(cov);
    Real nErr1 = Copy(NError);
    Real nWar1 = Copy(NWarning);
    Real Show(vrb,"ALL");
    Real _lastVerbose := vrb;
    If(Or(nErr1>nErr0,nWar1>nWar0),cov*0,L)
  };

  ////////////////////////////////////////////////////////////////////////////
  Real initialize(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    If(IsUnknown(_.K), {
      Real _.K := Rows(_.acor);
      Real _.Ks := Card(_.sel);
      Real If(!_.Ks, Set _.sel := Range(1,_.K,1); Real _.Ks := _.K);
      Matrix _.acor := SubRow(_.acor_full,_.sel);
      Matrix _.samplingBartlett.cov := ARMA.ACF.Bartlett.Cov(_.acor,_.K,_.N);
      Matrix If(_.Ks<_.K, _.samplingBartlett.cov := 
        SubCol(SubRow(_.samplingBartlett.cov,_.sel),_.sel));
      Matrix  _.samplingBartlett.cov_L := SafeCholesky(_.samplingBartlett.cov);
      True
    });    
    Real _.varNum := _.p+_.q;
    Real _.logDetL := MatSum(Log(SubDiag(_.samplingBartlett.cov_L,0)));
    Real If(IsUnknown(_.logDetL), _.logDetL := -1/0);
    Set _.arima := [[ @ARIMAStruct(
      1,RandStationary(_.p,1),RandStationary(_.q,1),1) ]];
    Real initialize.ArimaVector(?);
    Real If(!Card(config), setDefaultConfig(?) );
    Real set.config(?);
    True
  };

  ////////////////////////////////////////////////////////////////////////////
  Real set.prior.NonInf(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    Real _.priorLogDens := 0
  };

  ////////////////////////////////////////////////////////////////////////////
  Real set.prior.InvStatVol(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    Real _.priorLogDens := 
      - UnitRoot::LogVRegion(_.p)
      - UnitRoot::LogVRegion(_.q)
  };

  ////////////////////////////////////////////////////////////////////////////
  Real set.prior.BIC(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    Real n = _.p+_.q;
    Real _.priorLogDens := -0.5*n*Log(_.N)
  };

  ////////////////////////////////////////////////////////////////////////////
  Real set.prior.AIC(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    Real n = _.p+_.q;
    Real _.priorLogDens := -n
  };

  ////////////////////////////////////////////////////////////////////////////
  Real set.prior.AICc(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    Real n = _.p+_.q;
    Real _.priorLogDens := -n*_.N/(_.N-n-1)
  };

  ////////////////////////////////////////////////////////////////////////////
  Real set.config(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    Real _.samplingBartlett.exact := $config::acfCov=="TheoricalBartlett";
    Text cfg = $config::priorType;
    Eval("set.prior."+cfg+"(?)")
  };


  ////////////////////////////////////////////////////////////////////////////
  Matrix calcStdRes.Diagonal(Matrix tacor)
  ////////////////////////////////////////////////////////////////////////////
  {
    (tacor-_.acor)*Sqrt(1/_.N)
  };

  ////////////////////////////////////////////////////////////////////////////
  Matrix calcStdRes.CommutedBarlett(Matrix tacor)
  ////////////////////////////////////////////////////////////////////////////
  {
    LTSolve(_.samplingBartlett.cov_L,tacor-_.acor)
  };

  ////////////////////////////////////////////////////////////////////////////
  Matrix calcStdRes.TheoricalBartlett(Matrix tacor)
  ////////////////////////////////////////////////////////////////////////////
  {
    Matrix cov = ARMA.ACF.Bartlett.Cov(tacor,_.K,_.N);
    Matrix If(_.Ks<_.K, cov := SubCol(SubRow(cov,_.sel),_.sel));

    Matrix cov_L = SafeCholesky(cov);
    Real _.logDetL := MatSum(Log(SubDiag(cov_L,0)));
    Real If(IsUnknown(_.logDetL), _.logDetL := -1/0);
    LTSolve(cov_L, tacor-_.acor)
  };

  ////////////////////////////////////////////////////////////////////////////
  Matrix calcStdRes.config(Matrix tacor)
  ////////////////////////////////////////////////////////////////////////////
  {
    Text cfg = $config::acfCov;
    Eval("calcStdRes."+cfg+"(tacor)")
  };

  ////////////////////////////////////////////////////////////////////////////
  Real pi.logDens(Matrix y)
  ////////////////////////////////////////////////////////////////////////////
  {
    Set arima = vector2arima(y);
  //WriteLn("TRACE [pi.logDens] "+Name(_this)+" : "+_.label);
    Polyn ar = ARIMAGetAR(arima);
    Polyn ma = ARIMAGetMA(arima);
    Real nErr0 = Copy(NError);
    Matrix tacov = ARMATACov(ar, ma, _.K+1);
  //WriteLn("TRACE [pi.logDens] tacov="<<Matrix Tra(tacov));
    Real nErr = Copy(NError)-nErr0;
    Real llh = If(nErr,-1/0,
    {
      Matrix tacor = SubRow(Sub(tacov,2,1,_.K,1)*1/MatDat(tacov,1,1),_.sel);
    //WriteLn("TRACE [pi.logDens] tacor="<<Matrix Tra(tacor));
      Matrix e = calcStdRes.config(tacor);
    //WriteLn("TRACE [pi.logDens] e="<<Matrix Tra(e));
      -0.5*_.Ks*Log(2*PI)-0.5*MatSum(e$*e) - _.logDetL + _.priorLogDens
    });
    Real If(!IsUnknown(llh) & GT(llh,_.llh.max),
    {
      Matrix _.mle := y;
      _.llh.max := llh
    });
    llh
  };


  ////////////////////////////////////////////////////////////////////////////
  Real goToMaxLikelihood(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    If(!Or(_.p,_.q),True, {
      @NameBlock aref = [[_this]]; 
      NameBlock optimizer = NonLinGloOpt::@PipeLine::undefined(?);
      Matrix optimizer::x0 := If($config::usePreliminarEstimation,
      {
        Set prlm = ARMAPreliminaryEstimation(Col(1)<<_.acor_full,_.p,_.q,_.N,1000*_.q,0.1/Sqrt(_.N));
        Matrix old.x = arima2vector(_.arima);
        Real old.llh = pi.logDens(old.x);
        Polyn (_.arima[1])->AR := InverseNonStationaryRoots(prlm::phi);
        Polyn (_.arima[1])->MA := InverseNonStationaryRoots(prlm::theta);
        Matrix prlm.x = arima2vector(_.arima);
        Real prlm.llh = pi.logDens(prlm.x);
        If(prlm.llh>old.llh,prlm.x,old.x)
      },
      {     
        arima2vector(_.arima)
      });
      Real optimizer::set_problem(NonLinGloOpt::@Problem problem = [[
        Real sign = 1;
        Real neededGlobal = False;
        Real id_analyticalClass = 2;
        Real id_useGradient = True;
        Real n = $aref::_.varNum;
        Anything target = [[ $aref, $aref::log_lk_target ]];
        Set inequations = Copy(Empty)
      ]]);
      Set EvalSet(Tokenizer($config::optMethod,","),Real(Text optMet)
      {
        NonLinGloOpt::@Method method =
        [[
          Real id_algorithm = NonLinGloOpt::Algorithm[optMet]
        ]];
        Real optimizer::add_method(method);
        @NameBlock sc = [[method::stopCriteria]];
        Real $sc::maxTime := $config::optMaxTime;
        Real $sc::absoluteTolerance_target := $config::optAbsoluteTolerance_target;
        Real $sc::relativeTolerance_target := $config::optRelativeTolerance_target;
        Real $sc::relativeTolerance_var := $config::optRelativeTolerance_var;
        True
      });
      Real vrb = _lastVerbose;
      Real _lastVerbose := $config::optVerbose;
      Real Show(_lastVerbose,"ALL");
      Real Show(_lastVerbose,"E");
      Real optimizer::optimize(False);
      Real Show(vrb,"E");
      Real Show(vrb,"ALL");
      Real _lastVerbose := vrb;
      Set _.arima := vector2arima(_.mle);
      Matrix _.x := _.mle;
      Matrix _.asymtotic.x := _.x;
      Real n = _.varNum;
      Real _.llh.max := pi.logDens(_.x);
      Matrix G = Gradient(pi.logDens,_.x);
      Matrix H = -Hessian(pi.logDens,_.x);
      If(Or(Rows(H)!=_.varNum,Columns(H)!=_.varNum,HasUnknown(H)), False,
      { 
        Set svd = SVD(H);
        Matrix U  = svd[1];
        Matrix D2 = svd[2];
        Matrix V  = svd[3];
        Matrix D  = RPow(D2,0.5);
        Real _.asymtotic.S.logDet := -2*MatSum(Log(SubDiag(D,0)));
        Matrix Di = InverseDiag(D);
        Matrix _.asymtotic.S.Lt := V*Di;
        True
      })
    })
  };

  ////////////////////////////////////////////////////////////////////////////
  Real pi.logDens.avr.PriorSampling(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
/*
    WriteLn(_MID+"Simulating by Prior-Sampling "<<
      $config::sampleLength+" times Log-likelihood of ACF for "
      "ARMA("<<_.p+","<<_.q+")...");
*/
    Matrix _.mcmc.llh := ARMA.ACF.Bartlett.LLH.RandStationary(
      _.p,_.q,_.acor,_.N,$config::sampleLength, _.samplingBartlett.exact) 
      + _.priorLogDens;
    Real _.llh.max := MatMax(_.mcmc.llh);
    Matrix _.mcmc.prob := Exp(_.mcmc.llh-_.llh.max);
    Real _.llh.avr := Log(MatSum(_.mcmc.prob))+_.llh.max;
    Copy(_.llh.avr)
  };

  ////////////////////////////////////////////////////////////////////////////
  Real pi.logDens.avr.ImportanceSampling(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
/*
    WriteLn(_MID+"Simulating by Importance-Sampling "<<
      $config::sampleLength+" times Log-likelihood of ACF for "
      "ARMA("<<_.p+","<<_.q+")...");
*/
    Real goToMaxLikelihood(?);
    Real n = _.varNum;
    If(Or(Rows(_.asymtotic.S.Lt)!=_.varNum,
          Columns(_.asymtotic.S.Lt)!=_.varNum,
          HasUnknown(_.asymtotic.S.Lt)), -1/0,
    { 
      Matrix _.mcmc := Constant(0,n,?);
      Matrix pql.sample = SetMat(For(1,$config::sampleLength,Set(Real iter)
      {
        Matrix e = Gaussian(n,1,0,1);
        Matrix z = _.asymtotic.x + _.asymtotic.S.Lt * e;
        Real AppendRows(_.mcmc, Tra(z));
        [[
          Real q = -0.5*n*Log(2*Pi)-0.5*_.asymtotic.S.logDet-0.5*MatSum(e^2);
          Real p = pi.logDens(z);
          Real l = p-q
        ]]
      }));
      Matrix w = -SubCol(pql.sample,[[1]])+_.priorLogDens;
      Real w.max_ = MatMax(w);
      Matrix w.prob = Exp(w-w.max_);
      Real w.sum = Log(MatSum(w.prob))+w.max_;
      Matrix _.mcmc.llh := SubCol(pql.sample,[[3]]) - w.sum;
      Real llh.max_ = MatMax(_.mcmc.llh);
      Matrix _.mcmc.prob := Exp(_.mcmc.llh-llh.max_);
      Real _.llh.avr := (Log(MatSum(_.mcmc.prob))+llh.max_);
      Copy(_.llh.avr)
    })
  };

  ////////////////////////////////////////////////////////////////////////////
  Real pi.logDens.avr.MetropolisHastings(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    Text _MID = "[@ARMA.Sampler.ACF.Reg]";
    If($config::optIter,
    {
      WriteLn(_MID+"Searching optimum along "<<$config::optIter+
        " iterations of Log-likelihood of ACF for "
        "ARMA("<<_.p+","<<_.q+")...");
      Real goToMaxLikelihood(?)
    });
    WriteLn(_MID+"Simulating by Metropolis-Hastings "<<
      $config::sampleLength+" times Log-likelihood of ACF for "
      "ARMA("<<_.p+","<<_.q+")...");
    Real _build_mcmc($config::sampleLength);
    Real burnin = Floor($config::burninRatio*$config::sampleLength);
    Set rowIdx = Range(burnin+1,$config::sampleLength,1);
    Matrix llh=SubRow(_.mcmc.llh,rowIdx);
    Real _.llh.avr := Log(MatAvr(Exp(llh-_.llh.max)))+_.llh.max;
    Copy(_.llh.avr)
  };

  ////////////////////////////////////////////////////////////////////////////
  Real pi.logDens.avr(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    Real set.config(?); 
    Case(
    And(!_.p,!_.q),pi.logDens(Constant(0,0,?)),
    _.Ks<_.varNum,-1/0,
    1==1,
    {
      Text cfg = $config::sampler;
      Eval("pi.logDens.avr."+cfg+"(?)")
    })
  };

  ////////////////////////////////////////////////////////////////////////////
  Real log_lk_target(Matrix y, Matrix grad)
  ////////////////////////////////////////////////////////////////////////////
  {
    Real z = pi.logDens(y);
    Matrix If(Rows(grad), grad:=Gradient(pi.logDens, y));
    z
  };

  ////////////////////////////////////////////////////////////////////////////
  Real clear(Real void)
  ////////////////////////////////////////////////////////////////////////////
  { 
    Set config := Copy(Empty);
    True
  };

  ////////////////////////////////////////////////////////////////////////////
  Real __destroy(Real void)
  ////////////////////////////////////////////////////////////////////////////
  { 
    clear(void)
  }
};
